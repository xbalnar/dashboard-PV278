import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import LandingPage from './views/LandingPage';
import { useAuth0 } from './react-auth0-spa';
import { makeStyles } from '@material-ui/core/styles';
//import CssBaseline from '@material-ui/core/CssBaseline'; uncomment if you want to use material UI
import HomePage from './views/HomePage';
import Loading from './components/Loading';

const drawerWidth = 240;

import { legacy_createStore as createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

const exampleReducer = (state = {}, action) => {
  return (
    {
      foo: {
        ...state,
        ...action.payload,
      },
      DATA_FETCHED: {
        ...state,
        data: action.payload,
      },
    }[action.type] || state
  );
};

const store = createStore(combineReducers({ exampleReducer }), applyMiddleware(thunk));

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    background: theme.palette.background.default,
    ...theme.typography.body1,
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

const App = () => {
  const { loading } = useAuth0();
  const classes = useStyles();

  if (loading) {
    return <Loading />;
  }

  return (
    <div className={classes.root}>
      <Provider store={store}>
        <BrowserRouter>
          {/* <CssBaseline />  */} {/* uncomment if you want to use material UI */}
          <main className={classes.content}>
            <Routes>
              <Route path="/home-page" element={<PrivateRoute component={HomePage} />} />
              <Route index path="/" element={<LandingPage />} />
            </Routes>
          </main>
        </BrowserRouter>
      </Provider>
    </div>
  );
};

export default App;
